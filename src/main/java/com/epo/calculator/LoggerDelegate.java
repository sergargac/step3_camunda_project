package com.epo.calculator;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import java.util.logging.Logger;

public class LoggerDelegate implements JavaDelegate {

    private final Logger LOGGER = Logger.getLogger(LoggerDelegate.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        boolean correct = (boolean) delegateExecution.getVariable("correct");
        if(correct){
            LOGGER.info("The received result is correct!");
        }else{
            LOGGER.info("The received result is not correct!");
        }

    }
}
