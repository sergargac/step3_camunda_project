package com.epo.calculator;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class Multiply implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        Long valueA = (long) delegateExecution.getVariable("valueA");
        Long valueB = (long) delegateExecution.getVariable("valueB");

        double result = valueA * valueB;

        delegateExecution.setVariable("result", result);

    }

}
